# Taller de Análisis Numérico - Diferencias Finitas

Este repositorio contiene el código y los resultados obtenidos durante el taller de cálculo numérico.

## Contenido



1. **Relación entre Newton-Raphson y el Método de la Secante:**
   En este primer punto, se explora la relación entre el método de Newton-Raphson y el método de la secante para la aproximación de raíces de funciones. Se demuestra cómo, al reemplazar la derivada exacta por una aproximación particular basada en diferencias finitas, se obtiene el método de la secante como una variante del método de Newton-Raphson.

2. **Métodos de Diferencias Finitas para la Evaluación del Error:**
   En este punto se presenta un caso de estudio ejemplificado del uso de los métodos de diferencias finitas progresivas y centradas para la evaluación del error en relación con un valor de \( h \) dado. Se muestran implementaciones de ambas aproximaciones y se comparan los resultados obtenidos con la derivada exacta de la función.

3. **Determinación del \( h \) Mínimo y su Relación con el Error Numérico:**
   Se aborda el concepto del \( h \) mínimo en el contexto de los métodos numéricos, particularmente en el uso de diferencias finitas para la aproximación de derivadas. Se demuestra cómo el \( h \) mínimo está relacionado con el error numérico y se proporciona una demostración de su cálculo. Además, se discute la importancia de seleccionar un \( h \) apropiado para minimizar el error en las aproximaciones numéricas.

4. **Aplicaciones Prácticas y Demostración de la Relación con el \( h \) Mínimo:**
   En este punto se presentan aplicaciones prácticas del concepto de \( h \) mínimo y su relación con el error numérico. Se muestra cómo el \( h \) mínimo se puede calcular y utilizar en la práctica para garantizar la precisión de los cálculos numéricos, especialmente en situaciones donde se requiere una alta precisión.
